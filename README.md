# BULLETINI #

Send Tailored Wordpress Content To Your ONTRAPORT Contacts
A simple to use plugin that brings relevant content from your Wordpress Blog into ONTRAPORT contact records ready to be merged into a beautifully designed bulletin newsletter.

## Settings ##

### General ###
### License ###

Enter your license to begin. Click Here to buy a license or upgrade your account.
Don't have a paid license yet or want to up but want to test it out? No problem. Use the code FREEPLAN to unlock the Free Bulletini Plan on your site which enables you to run 100 contact syncs per calendar month. 

What is a Contact Sync? - Each time that you merge content from your blog into a single contact in ONTRAPORT this is counted as a content sync. If you have 500 contacts and you want to send new content to them 4 times in a calendar month you will require an account with 2000 or more Contact Syncs. Once you hit your monthly limit the Contact Sync Script will be disabled until the start of the next calendar month.

[ Enter Code ]
(once code is validated the next section opens)

### Connect to ONTRAPORT ###

You can get these details from your ONTRAPORT account. Login to your account and click on the Administration link from the dropdown at top right.

[ APP ID ]

[ API Key ]

### Content Settings ###

Bulletini takes specific parts of your content and merges it into new fields that we will dynamically create in your ONTRAPORT acocunt. Each piece of content requires 5 field. Title, Photo, Description, Author and Link. We have templates that can have 4, 7 or 9 pieces of content merged. 

How many pieces of content do you want in each newsletter?
[ 4 (22 fields), 7 (37 fields), 9 (47 fields) ]

### Contact Filter ###

Each time that Bulletini runs the content script it can add the content only to contacts that have a particular tag. You can leave this empty to have no tag filter.

What tag would you like to use as a filter?
[ Tag list or create new tag ]

## ONTRAPORT Fields ##

We are now ready to create the fields in your ONTRAPORT account. The fields that will be created correlate with the content meta data that will be able to be merged into the Newsletter. Here are a list of the Fields and Types:

### Content Fields: ###
* Author  - Text
* Title - text
* Image - Text
* Description - Long Text
* Link - text
* Settings Fields.
* Interests - List Selection

To list all the interests that your contacts can select relating to your content taxonomy
Past Posts - Long Text

The WP Post numbers that have already been sent to a user will display here to ensure that the same content is not sent a second time.

Click the button below to create the fields.
[ Create and Setup Fields Now! ]

(ONTRAPORT returns an error message due to there not being enough space for new fields, we need to remove the fields that have been created and then allow the user to select less fields eg. option 4 above.)

Error Message: 'Error: It looks like you don't have enough fields available in your account for use to create new fields in your account. You can change your settings above to use less fields or log into your account and remove any redundant fields you may have'

Success Message: 'Success: The fields have been successfully created in your Account.'

## Content Settings ##

In this section you are able to decide which post or page type from your WP site you will be using in Bulletini. If you have custom fields in your content then you can decide which fields you want to be using for each piece of content.

### Post Type ###
[ Post ]
This will be post type which will be sent in the newsletter.

### Add Featured Article Field? ###
[ Y / N ]
Select yes to add a Featured Field in your selected post type add/edit screen. You can then assign any one of your posts as the featured article that will be served in your Newsletter.

### Filtering Content for your Contacts ###
So that we can deliver content that is relevant for each of your contacts we need to have a relationship between what they are interested in and how your articles are categorized. Bulletini allows you to do this in two ways.
1. Using the new Interests List Field in ONTRAPORT by adding Category names as options that your contacts can select from. For example if you already have 'Monkey' 'Dog' and 'Dance'  as categories for your posts, then you simply add these as options in the [Interests] field in ONTRAPORT and assign to each contact their interests,
2. Custom Interests - This option allows you to create new Super Categories that can be assigned to any one of your categories. For example the new Super Category could be 'Animals' and you then can assign this Custom Interest to the 'Monkey' and 'Dog' Category that you already have assigned to your blog posts. 

### Would you like to use New Custom Interests to be added to your Category add/edit screen? ###
[ Yes / No ]

(Only show next section if Yes is chosen)

### Custom Interests ###
[ Interest 1, Interest 2 ]
Add interests here. Each in new line.

[SAVE] - On save it automatically adds these options to the OP field. 
Important: These new options will be added to the Custom Interests field in your ONTRAPORT account. If you edit or delete that field in ONTRAPORT it will break the connection with Wordpress that keeps this in sync.

## Content Fields ##
Each piece of content that is used in your Newsletter can have 5 fields associated. Title, Photo, Description, Author, Link.
Use the dropdowns below to choose which Wordpress Data point you want to associate to each of the 5 fields in ONTRAPORT.
[ Title ]
[ Image ]
[ Description ] 
[ Author ]
[ Link ]
[ Save Changes ]

### Enable UTM Tracking? ###
Each of the links in your Newsletter can have UTM tracking tags added that are article specific.
[Yes or No]

utm_source=bulletini
utm_medium=email
utm_campaign=YYMMDD eg. 160915 for 15th of September 2016
utm_content=1234 this is the WP post number
utm_term=A1 (this is in reference to the placement of the link within the newsletter)

## ONTRAPORT Field Settings ##
This section lists all the content data that will be passed to contacts and which corresponding ONTRAPORT field will be used to house that data. Bulletini automatically selects the corresponding fields but you can override this by changing to any other field from the dropdown.
[ Fields listed ]
[ Save Changes ]

## Newsletter Templates ##
Bulletini takes content data from your WP page and adds these data points into ONTRAPORT contact fields based on their interests. These fields can then be used to Merge into Messages just like any other ONTRAPORT Field. You can create your own designed templates, use an existing ONTRAPORT template or Push one of our templates into your Account using the options below.
 
### Template 1 - Default ###
This is a very basic table structure that can be a great starting point for further design work.
[ Settings ] [ Edit Message ] [ Uninstall ]

### Template 2 - Cordelia ###
1 Feature Article, 3 Medium Articles and 5 Smaller Articles. All with photos.
[ Settings ] [ Edit Message ] [ Uninstall ]

### Template 3 - Desdemona ###
2 Feature Article Sections, 4 Medium Articles. All with photos. Social Media Icons.
[ Settings ] [ Edit Message ] [ Uninstall ]

### Template 4 - Portia ###
2 Feature Article Sections, 4 Medium Articles. All with photos. Social Media Icons.
[ Settings ] [ Edit Message ] [ Uninstall ]
[ Save Changes ]

## Content Sync Script ##
Now that all the settings have been configured we can initiate the script that will bring the content data into ONTRAPORT fields.

This can be done on Demand by Clicking the button below.
[ Click Here to Initiate Bulletini Script Now ]

If you would like this to happen automatically set up a cron job to run the script on specific intervals.

Cron Runs: Select one
[Daily, Weekly, Monthly]

IF Daily > Runs every [1,2,3,4,5,6,7,8,9,10...30] days
Starts on [Date Selector] (eg. Every 2 days starting on September 2nd 2016)

IF Weekly > Runs every [1,2,3,4,5...20] weeks
Run on [M] [T] [W] [Th] [F] [S] [Su] (Check box)
Starts on [Date Selector] (eg. Weekly on Tuesday, Friday and starting on September 2nd 2016)

IF Monthly > Runs every [1,2,...12] months
By: Day of Month / Day of Week

IF > DoM - Starts on [Date Selector] (eg. Monthly on day 16th of month)
IF > DoW - Starts on [Date Selector] (eg. Monthly on third Monday of month)

Confirm Rule
[ Save Cron to Run at above interval ]
 
## Advanced ##
### ONTRAPORT ###

Click the button to create and setup the fields required for the plugin. (Note: All the previous settings will be overridden).

[ Re-create and Setup Fields Now! ]

If you woudl like to remove Bulletini from your ONTRAPORT account use the button below to remove all messages and fields assocaited with this plugin.

[ Remove all Bulletini Messages and Fields Now! ] - Pop up - Are you sure that you want to remove all Bulletini fields and messages from your account? Y or N 

Warning: This is permanent. You will need to begin again if you use this option.
Cron Setup

For better performance, Disbale WP Cron and setup manual cron job. Read this article Properly Setting Up WordPress Cron Jobs

Your Action link:
http://wamthost.com/clients/itmooti/dng/?feed=sync_userdata

### Clear Cache ###

Click the button to clear the cache of Tags and Contact Fields.
[ Clear Cache Now! ]

### Support ###
Do you need help? Check out our knowledge base HERE.