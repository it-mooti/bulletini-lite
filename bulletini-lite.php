<?php
/*
Plugin Name: Bulletini Lite
Plugin URI: http://www.itmooti.com
Description: Send Tailored WordPress Content To Your ONTRAPORT Contacts
Version: 1.1
Author: ITMOOTI
Author URI: http://www.itmooti.com
*/


//Dependent Classes
require_once('inc/class.plugin.php');
require_once('inc/class.ontraport.php');
require_once('inc/class.settings.php');
require_once('inc/class.bulletini-lite_updater.php');

//Main Class
class bulletiniLite{
	//Private Instances for other classes
	private $ontraport, $settings, $plugin_settings;
	
	//Constructor Function
	public function __construct(){
		
		//Create instances for dependent classes.
		$this->plugin_settings=new bulletini_lite_plugin();
		$this->settings=new bulletini_lite_settings();
		//Check for Updates
		if ( is_admin() ) {
			new bulletini_lite_updater( __FILE__, 'it-mooti', "bulletini-lite" );
		}
		
		//Adding WordPress init() function hook.
		add_action( 'init', array( $this, 'init' ) );
		
	}
	public function upgrade_install(){
		$baseUrl = 'https://bitbucket.org//$repository/get/master.zip';
		
		// store the zip file temporary
		$zipFile = 'full-' . time() . '-' . rand(0, 100);
		$zipLocation = $CONFIG['commitsFolder'] . (substr($CONFIG['commitsFolder'], -1) == DIRECTORY_SEPARATOR ? '' : DIRECTORY_SEPARATOR);
	
		
		$result = getFileContents($baseUrl . $repoUrl . $branchUrl, $zipLocation . $zipFile);
	}
	
	//WordPress init() function hook.
	public function init(){
		$app_id=bulletini_lite_option('app_id');
		$api_key=bulletini_lite_option('api_key');
		$this->ontraport=new bulletini_lite_ontraport($app_id, $api_key);
		add_action( 'wp_ajax_bulletini_lite_save_template_settings', array($this, 'save_template_settings'));
		if(isset($_POST["bulletini_lite_rehash"])){
			if(wp_verify_nonce( $_POST['bulletini_lite_settings_meta_box_nonce'], 'bulletini_lite_settings_meta_box' ) && current_user_can( 'manage_options')) {
				$this->ontraport=new bulletini_lite_ontraport($app_id, $api_key);
				$this->set_op_data();
			}
		}
		if(isset($_GET["bulletini_lite_install_template"])){
			if(isset($_GET['template']) && isset($_GET['bulletini_lite_install_template_nonce']) && wp_verify_nonce($_GET['bulletini_lite_install_template_nonce'], 'install-template_'.sanitize_title($_GET['template'])) && current_user_can( 'manage_options') ) {
				$this->install_template();
			}
		}
		if(isset($_GET["bulletini_lite_uninstall_template"])){
			if(isset($_GET['template']) && isset($_GET['bulletini_lite_uninstall_template_nonce']) && wp_verify_nonce($_GET['bulletini_lite_uninstall_template_nonce'], 'uninstall-template_'.sanitize_title($_GET['template'])) && current_user_can( 'manage_options') ) {
				$this->uninstall_template();
			}
		}
	}
	
	//Template Functions
	public function install_template(){
		if(isset($_GET["template"])){
			$template=sanitize_title($_GET["template"]);
			$bulletiniLite_template_message_id=bulletini_lite_option('template_'.$template.'_message_id');
			if(empty($bulletiniLite_template_message_id) || 1){
				if($this->ontraport->is_valid()){
					$template_directory=plugin_dir_path( __FILE__ ).'templates/'.$template;
					if(is_dir($template_directory)){
						$template_content=$this->get_template_content($template);
						//echo $template_content; die;
						$config_data=array();
						$config_file=$template_directory."/config.xml";
						if(file_exists($config_file)){
							$config_data=simplexml_load_string(file_get_contents($config_file));
							if(!$config_data){
								$config_data=array();
							}
							else{
								$config_data=(array)$config_data;
							}
						}
						$template_name="Bulletini - ".(isset($config_data["name"])?$config_data["name"]:$template);
						$message=$this->ontraport->Request('message', array("objectID"=>"7", "type"=>"e-mail", "alias"=>$template_name, "name"=>$template_name, "subject"=>'[Owner//Business Name] Newsletter',  "message_body"=>$template_content), true, "post");
						if(isset($message->id)){
							update_option('bulletini_lite_template_'.$template.'_message_id', $message->id);
							$this->add_notice('Template Installed Successfully. Click Edit Template button to edit it.', 'message', 'Template'.$template);
						}
						else{
							$this->add_notice('Error in creating message. Please Try Again or Contact Support', 'error', 'Template'.$template);
						}
					}
					else{
						$this->add_notice('Template Directory missing. Reinstall the plugin.', 'error', 'Template'.$template);
					}
				}
				else{
					$this->add_notice('APP ID or API Key is not valid. Please confirm and submit again.', 'error', 'Template'.$template);
				}
			}
			else{
				$this->add_notice('Template is already installed. Click Edit Template button to edit it within your ONTRAPORT account.', 'error', 'Template'.$template);
			}
		}
		else{
			$this->add_notice('Template name is empty. Try again.', 'error', 'Template'.$template);
		}
	}
	public function uninstall_template(){
		if(isset($_GET["template"])){
			$template=sanitize_title($_GET["template"]);
			$bulletiniLite_template_message_id=bulletini_lite_option('template_'.$template.'_message_id');
			if(!empty($bulletiniLite_template_message_id)){
				$message=$this->ontraport->Request('object', array("objectID"=>"7", "id"=>$bulletiniLite_template_message_id), false, "delete");
				delete_option('bulletini_lite_template_'.$template.'_message_id');
				$this->add_notice('Template uninstalled successfully.', 'message', 'Template'.$template);
			}
			else{
				$this->add_notice('Template not installed.', 'error', 'Template'.$template);
			}
		}
	}
	public function get_template_content($template){
		$template_directory=plugin_dir_path( __FILE__ ).'templates/'.$template;
		$config_data=array();
		$config_file=$template_directory."/config.xml";
		if(file_exists($config_file)){
			$config_data=simplexml_load_string(file_get_contents($config_file));
			if(!$config_data){
				$config_data=array();
			}
			else{
				$config_data=(array)$config_data;
			}
		}
		$template_content=file_get_contents($template_directory."/template.html");
		preg_match_all("/\[Condition[^\]]*\]/", $template_content, $merge_fields);
		foreach($merge_fields[0] as $merge_field){
			$conditional_content=explode($merge_field, $template_content);
			if(isset($conditional_content[1])){
				$conditional_content=explode(str_replace('[Condition', '[/Condition', $merge_field), $conditional_content[1]);
				$conditional_content=$conditional_content[0];
			}
			else
				$conditional_content="";
			$is_empty=true;
			if(!empty($conditional_content)){
				$variables=explode('|', trim(str_replace(array('[Condition:', ']'), array('', ''), $merge_field)));
				foreach($variables as $variable){
					$field_input_key='bulletini_lite_'.$template."_".str_replace("-", "_", sanitize_title($variable));
					$field_value=get_option($field_input_key);
					if(!empty($field_value)){
						$is_empty=false;
						break;
					}
				}
			}
			$replace_with='';
			if(!$is_empty){
				$replace_with=$conditional_content;
			}
			$template_content=str_replace($merge_field.$conditional_content.str_replace('[Condition', '[/Condition', $merge_field),$replace_with, $template_content);
		}
		$posts = '';
		preg_match_all("/\[Config[^\]]*\]/", $template_content, $merge_fields);
		if(isset($config_data["configuration"])){
			foreach($config_data["configuration"] as $field_type=>$field_key){
				$field_input_key='bulletini_lite_'.$template."_".str_replace("-", "_", sanitize_title($field_key));
				$field_value=get_option($field_input_key);
				switch($field_type){
					case "text":break;
					case "textarea": break;
					case "image":
						$img_src = wp_get_attachment_image_src( $field_value, 'full' );
						$field_value=$img_src[0];
					break;
					case "color":
						$bar_image=plugin_dir_path( __FILE__ ).'templates/'.$template."/images/bar.png";
						if(file_exists($bar_image) && !empty($field_value)){
							$img = imagecreatetruecolor(1,4);
							$color=$this->hex2rgb($field_value);
							imagesetpixel($img, 0, 0, imagecolorallocate($img, 255, 255, 255));
							imagesetpixel($img, 0, 1, imagecolorallocate($img, $color[0], $color[1], $color[2]));
							imagesetpixel($img, 0, 2, imagecolorallocate($img, $color[0], $color[1], $color[2]));
							imagesetpixel($img, 0, 3, imagecolorallocate($img, 255, 255, 255));
							imagepng($img, $bar_image);
						}
					break;
					case "posts":
						$posts = $field_value;
					break;
				}
				$template_content=str_replace(array('[Config:'.$field_key.']', '[Config: '.$field_key.']'), array($field_value, $field_value), $template_content);
			}
		}
		$template_content=str_replace(array(
			'[Option:Template URL]',
			'[Option: Template URL]'
		),
		array(
			plugins_url("/templates/".$template, __FILE__),
			plugins_url("/templates/".$template, __FILE__)
		),
		$template_content);
		if( !empty( $posts ) ) {
			$posts = explode( ',', $posts );
			$cnt_articles = 0;
			foreach( $posts as $post ) {
				$post = get_post( $post );
				$cnt_articles++;
				switch($cnt_articles){
					case 1: $article_number="A1"; break;
					case 2:case 3:case 4: $article_number="B".($cnt_articles-1); break;
					default: $article_number="C".($cnt_articles-4); break;
				}
				$post = $this->populate_post( $post );
				$template_content = str_replace( array(
					'['.$article_number.' Title]',
					'['.$article_number.' Description]',
					'['.$article_number.' Author]',
					'['.$article_number.' Image]',
					'['.$article_number.' Link]',
				), array(
					$post[ "title" ],
					$post[ "desc" ],
					$post[ "author" ],
					$post[ "image" ],
					$post[ "link" ],
				), $template_content );				
			}
		}
		return $template_content;
	}
	public function save_template_settings(){
		$return=0;
		if(current_user_can('manage_options')){
			if(isset($_POST["bulletini_lite_template"]) && !empty($_POST["bulletini_lite_template"])){
				$template=sanitize_title($_POST["bulletini_lite_template"]);
				foreach($_POST as $field_key=>$field_value){
					if(strpos($field_key, 'bulletini_lite_'.$template."_")!==false){
						update_option($field_key, esc_attr(sanitize_text_field($field_value)));
						$return=1;
					}
				}
				$bulletiniLite_template_message_id=bulletini_lite_option('template_'.$template.'_message_id');
				if(!empty($bulletiniLite_template_message_id)){
					$template_content=$this->get_template_content($template);
					$message=$this->ontraport->Request('message', array("objectID"=>"7", "id"=>$bulletiniLite_template_message_id, "message_body"=>$template_content), true, "put");
				}
			}
		}
		echo $return;
		wp_die();
	}
	
	//Utility Functions
	public function app_name(){
		return "Bulletini Lite";
	}
	public function populate_post($post){
		$post_data=array();
		foreach(array(
			"title"=>"Title",
			"image"=>"Photo",
			"desc"=>"Description",
			"author"=>"Author",
			"link"=>"Link",
		) as $k=>$v){
			$bulletiniLite_field=bulletini_lite_option($k.'_field');
			switch($bulletiniLite_field){
				case "post_title":
					$bulletiniLite_field_value=$post->post_title;
				break;
				case "post_content":
					$bulletiniLite_field_value = wp_trim_words( strip_tags( $post->post_content ), 35 );
				break;
				case "post_thumbnail":
					$image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large');
					$bulletiniLite_field_value=$image[0];
				break;
				case "post_author":
					$author=get_userdata($post->post_author);
					$bulletiniLite_field_value=$author->display_name;
				break;
				case "permalink":
					$bulletiniLite_field_value=get_permalink($post->ID);
				break;
				default:
					$bulletiniLite_field_value=get_post_meta($post->ID, $bulletiniLite_field, true);
			}
			$post_data[$k]=$bulletiniLite_field_value;
		}
		return $post_data;
	}
	public function hex2rgb($color) {
		$default = array(0,0,0);
		if(empty($color)){
			return $default; 
		}
		if ($color[0] == '#' ) {
			$color = substr( $color, 1 );
		}
		if (strlen($color) == 6) {
				$hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
		} elseif ( strlen( $color ) == 3 ) {
			$hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
		} else {
			return $default;
		}
		$output =  array_map('hexdec', $hex);	
		return $output;
	}
	public function array_flatten($array) { 
		if (!is_array($array)) { 
			return FALSE; 
		}
		$result = array(); 
		foreach ($array as $key => $value) { 
			if (is_array($value)) {
				$value=array_combine(
					array_map(create_function('$k', 'return "'.$key.'_".$k;'), array_keys($value))
					, $value
				);
				$result = array_merge($result, $this->array_flatten($value)); 
			} 
			else { 
				$result[$key]=str_replace("articles_", "", $key); 
			} 
		} 
		return $result; 
	}
	
	//Bridge Functions
	public function add_notice($text, $type='message', $location='general'){
		$this->settings->add_notice($text, $type, $location);
	}
	public function option($option_name, $apply_the_content=false, $default_value=""){
		return $this->settings->option($option_name, $apply_the_content, $default_value);
	}
	public function is_authenticated(){
		return $this->plugin_settings->is_authenticated();
	}
	public function is_valid(){
		return $this->ontraport->is_valid();
	}
	
}
//Initialization
$bulletiniLite=new bulletiniLite();

//Common Functions
function bulletini_lite_option($option_name, $apply_the_content=false, $default_value=""){
	global $bulletiniLite;
	return $bulletiniLite->option($option_name, $apply_the_content, $default_value);
}
function bulletini_lite_authenticated(){
	global $bulletiniLite;
	if(isset($bulletiniLite)){
		return $bulletiniLite->is_authenticated();
	}
}
function bulletini_lite_connected(){
	global $bulletiniLite;
	if(isset($bulletiniLite)){
		return $bulletiniLite->is_valid();
	}
}
function bulletini_lite_add_notice($text, $type='message', $location='general'){
	global $bulletiniLite;
	if(isset($bulletiniLite)){
		$bulletiniLite->add_notice($text, $type, $location);
	}
}
