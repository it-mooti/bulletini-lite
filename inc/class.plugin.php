<?php
class bulletini_lite_plugin{
	public function __construct(){
		$this->plugin_links=(object)array("support_link"=>get_option("bulletini_lite_support_link", ""), "license_link"=>get_option("bulletini_lite_license_link", ""));
		register_activation_hook(str_replace("inc", "",__DIR__).'bulletini-lite.php', array($this, 'plugin_activation'));
		add_action( 'admin_notices', array( $this, 'show_license_info' ) );
		if(isset($_POST["bulletini_lite_license_key"])){
			update_option("bulletini_lite_license_key", esc_attr($_POST["bulletini_lite_license_key"]));
			$this->plugin_authentication();
		}
		add_filter( 'plugin_action_links_' . 'bulletini/bulletini-lite.php', array($this, 'plugin_action_link'));
		add_filter( 'plugin_row_meta', array($this, 'plugin_meta_link'), 10, 2);
	}
	public function is_authenticated(){
		if(get_option("bulletini_lite_plugin_authenticated", "no")=="yes")
			return true;
		else
			return false;
	}
	public function plugin_activation(){
		$this->plugin_authentication();
	}
	public function plugin_authentication(){
		$this->url="http://app.itmooti.com/wp-plugins/oap-utm/api-new.php";
		$request= "plugin_links";
		$postargs = "plugin=bulletini-lite&request=".urlencode($request);
		$args=array(
			'method'=>'POST',
			'timeout'=>30,
			'body' => $postargs,
			'sslverify'=>false,
		);
		$response=wp_remote_request($this->url, $args);
		if ( is_wp_error( $response ) ) {
		   $error_message = $response->get_error_message();
		   bulletini_lite_add_notice("Something went wrong: $error_message", "error");
		} else {
			$result = json_decode($response["body"]);
			if (json_last_error() === JSON_ERROR_NONE) {
				if(isset($result->status) && $result->status=="success"){
					update_option("bulletini_lite_support_link", $result->message->support_link);
					update_option("bulletini_lite_license_link", $result->message->license_link);
				}
			}
			else{
				bulletini_lite_add_notice("Something went wrong: ".$response["body"], "error");
			}
		}
		$license_key=get_option('bulletini_lite_license_key', "");
		if(!empty($license_key)){
			$request= "verify";
			$postargs = "plugin=bulletini-lite&domain=".urlencode($_SERVER['HTTP_HOST'])."&license_key=".urlencode($license_key)."&request=".urlencode($request);
			$args=array(
				'method'=>'POST',
				'timeout'=>30,
				'body' => $postargs,
				'sslverify'=>false,
			);
			$response=wp_remote_request($this->url, $args);
			if ( is_wp_error( $response ) ) {
			   $error_message = $response->get_error_message();
			   bulletini_lite_add_notice("Something went wrong: $error_message", "error");
			} else {
				$result = json_decode($response["body"]);
				if (json_last_error() === JSON_ERROR_NONE) {
					if(isset($result->status) && $result->status=="success"){
						update_option("bulletini_lite_plugin_authenticated", "yes");
						if(isset($result->message))
							update_option("bulletini_lite_message", $result->message);
					}
					else if(isset($result->status) && $result->status=="error"){
						update_option("bulletini_lite_plugin_authenticated", "no");
						if(isset($result->message))
							update_option("bulletini_lite_message", $result->message);
					}
				}
				else{
					bulletini_lite_add_notice("Something went wrong: ".$response["body"], "error");
				}
			}
		}
		else{
			update_option("bulletini_lite_plugin_authenticated", "no");
			update_option("bulletini_lite_message", "Please enter valid license key");
		}
	}
	public function show_license_info(){
		$license_key=get_option('bulletini_lite_license_key', "");
		if(empty($license_key)){
			echo '<div class="updated">
        		<p><strong>BULLETINI LITE:</strong> How do I get License Key?<br />Please visit this URL <a href="'.$this->plugin_links->license_link.'" target="_blank">'.$this->plugin_links->license_link.'</a> to get a License Key .</p>
	    	</div>';
		}
		$message=get_option("bulletini_lite_message", "");
		if($message!=""){
			echo '<div class="error">
        		<p><strong>BULLETINI LITE:</strong> '.$message.'</p>
	    	</div>';
		}
	}
	function plugin_action_link( $links ) {
		return array_merge(
			array(
				'settings' => '<a href="options-general.php?page=bulletini-lite">Settings</a>',
				'support_link' => '<a href="'.$this->plugin_links->support_link.'" target="_blank">Support</a>'
			),
			$links
		);
	}
	function plugin_meta_link( $links, $file ) {
		$plugin = plugin_basename(__FILE__);
		if ( $file == $plugin ) {
			return array_merge(
				$links,
				array(
					'settings' => '<a href="options-general.php?page=bulletini-lite">Settings</a>',
					'support_link' => '<a href="'.$this->plugin_links->support_link.'" target="_blank">Support</a>'
				)
			);
		}
		return $links;
	}
}