<?php
class bulletini_lite_settings{
	private $post_fields=array(), $op_tags;
	public $notices=array();
	public function __construct(){
		add_action('init', array($this, 'init'));
	}
	public function init(){
		$this->save_settings();
		add_action('admin_enqueue_scripts', array($this, 'admin_scripts'));
		add_action('admin_menu', array($this, 'menu'));
		add_action( 'wp_ajax_bulletini_lite_get_post_title', array($this, 'bulletini_lite_get_post_title'));
		add_action( 'wp_ajax_bulletini_lite_get_posts', array($this, 'bulletini_lite_get_posts'));
	}
	function bulletini_lite_get_post_title(){
		if( isset( $_POST[ "id" ] ) ) {
			$post = get_post( absint( $_POST[ "id" ] ) );
			if( $post ) {
				echo $post->post_title;
			}
		}
		wp_die();
	}
	function bulletini_lite_get_posts(){
		if( isset( $_POST[ "s" ] ) ) {
			$args = array( 'post_type' => bulletini_lite_option('post_type', false, 'post'), 's' => sanitize_text_field( $_POST[ "s" ] ) ) ;
			if( isset( $_POST[ "exclude" ] ) ) {
				$args[ 'exclude' ] = sanitize_text_field( $_POST[ "exclude" ] );
			}
			$posts = get_posts( $args );
			$response = array();
			foreach( $posts as $post ){
				$response[] = array(
					'id' => $post->ID,
					'title' => $post->post_title
				);
			}
			echo json_encode( $response );
		}
		wp_die();
	}
	public function admin_scripts() {
        wp_enqueue_style( 'bulletini_lite_admin_style', plugin_dir_url(__FILE__)."admin/style.css");
		wp_enqueue_style( 'bulletini_lite_admin_datetime', plugin_dir_url(__FILE__)."admin/datetime.css" );
		wp_enqueue_style( 'bulletini_lite_admin_fancybox', plugin_dir_url(__FILE__)."admin/fancybox/jquery.fancybox.css" );
		wp_enqueue_style('jquery-style', '//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css');
		wp_enqueue_style( 'bulletini_lite_chosen_style', plugin_dir_url(__FILE__)."admin/chosen/chosen.min.css");
		wp_enqueue_style( 'wp-color-picker' );
		wp_enqueue_script( 'bulletini_lite_admin_datetime', plugin_dir_url(__FILE__)."admin/datetime.js", array('jquery-ui-core', 'jquery-ui-slider', 'jquery-ui-datepicker', 'jquery-ui-selectmenu', 'wp-color-picker') );
		wp_enqueue_script( 'bulletini_lite_chosen_style', plugin_dir_url(__FILE__)."admin/chosen/chosen.jquery.min.js");
		wp_enqueue_script( 'bulletini_lite_admin_fancybox', plugin_dir_url(__FILE__)."admin/fancybox/jquery.fancybox.pack.js");
		wp_enqueue_script( 'bulletini_lite_angular', plugin_dir_url(__FILE__)."admin/angular.min.js");
		wp_enqueue_script( 'bulletini_lite_admin_script', plugin_dir_url(__FILE__)."admin/js.js");
		wp_localize_script( 'bulletini_lite_admin_script', '$bulletini_lite_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php')));
		wp_enqueue_media();
    }
	public function menu(){
        // This page will be under "Settings"
        add_menu_page(
			'Bulletini Lite', 
            'Bulletini Lite', 
            'manage_options', 
            'bulletini-lite', 
            array( $this, 'create_admin_page' ),
			plugins_url('admin/images/logo-icon.png', __FILE__)
        );
    }
	public function save_settings(){
		if(count($_POST)>0 && isset($_POST["submit"])){
			if ( ! isset( $_POST['bulletini_lite_settings_meta_box_nonce'] ) ) {
				return;
			}
			if ( ! wp_verify_nonce( $_POST['bulletini_lite_settings_meta_box_nonce'], 'bulletini_lite_settings_meta_box' ) ) {
				return;
			}
			if ( ! current_user_can( 'manage_options' ) ) {
				return;
			}
			//Check if API info has changed?
			$app_id=bulletini_lite_option('app_id');
			$api_key=bulletini_lite_option('api_key');
			if(isset($_POST["bulletini_lite_app_id"]) &&  isset($_POST["bulletini_lite_api_key"])){
				if(!empty($_POST["bulletini_lite_app_id"]) && !empty($_POST["bulletini_lite_app_id"])){
					if($app_id!=$_POST["bulletini_lite_app_id"] || $app_id!=$_POST["bulletini_lite_api_key"]){
						$ontraport=new bulletini_lite_ontraport(sanitize_text_field($_POST["bulletini_lite_app_id"]), sanitize_text_field($_POST["bulletini_lite_api_key"]));
						if(!$ontraport->is_valid()){
							$this->add_notice('APP ID or API Key is not valid. Please confirm and submit again.', 'error', 'ConnectToONTRAPORT');
						}
					}
				}
				else{
					$this->add_notice('Enter APP ID and API Key to continue.', 'error', 'ConnectToONTRAPORT');
				}
			}
			foreach($_POST as $k=>$v){
				if(strpos($k, 'bulletini_lite_')!==false){
					$v=$this->sanitize_text_field($v);
					update_option($k, $v);
				}
			}
			$this->add_notice('Settings saved successfully.', 'message');
		}
	}
	public function sanitize_text_field($value){
		if(is_array($value)){
			$return=array();
			foreach($value as $k=>$v){
				$return[$k]=$this->sanitize_text_field($v);
			}
			return $return;
		}
		else{
			return sanitize_text_field($value);
		}
	}
	public function create_admin_page(){
		?>        
        <div class="wrap">
        	<form method="post" class="bulletini-fields not-expanded">
           	  	<?php wp_nonce_field( 'bulletini_lite_settings_meta_box', 'bulletini_lite_settings_meta_box_nonce' );?>
                <h3><img src="<?php echo plugins_url('admin/images/header_logo.png', __FILE__);?>" class="bulletini_lite_header_logo"><span></span></h3>
                <div class="info_bar">
                	<?php submit_button();?>
              	</div>
                <?php $this->print_notices('general')?>
                <ul class="left-links">
                	<li class="menu-license"><a href="#content-license">License</a></li>
					<?php
					$app_id=bulletini_lite_option('app_id');
					$api_key=bulletini_lite_option('api_key');
					$ontraport=new bulletini_lite_ontraport($app_id, $api_key);
                    if(bulletini_lite_authenticated()){
						?>
						<li class="menu-api"><a href="#content-configuration">API Configuration</a></li>
                        <?php
                        if($ontraport->is_valid()){
							?>
                            <li class="menu-content-type"><a href="#content-content-settings">Content Type Settings</a></li>
                            <li class="menu-templates"><a href="#content-templates">Templates</a></li>
                            <?php
						}
					}
					?>
                    <li class="menu-support"><a href="https://itmooti.helpdocs.com/bulletini" target="_blank" class="bulletini_lite_external_link">Support</a></li>
                </ul>
                <div class="right-content">
                	<div id="content-license" class="content-elements">
                    	<h4>License</h4>
    	            	<div class="form-table">
                            <?php $bulletini_lite_license_key=bulletini_lite_option('license_key');?>
                            <label>License Key</label>
                            <small>Enter your license to begin. <a href="http://bulletini.rocks" target="_blank">Click Here to buy a license or upgrade your account</a>. <br /><br />
                                Do you want to test out Bulletini before you buy a license? No problem. Use the code FREETRIAL to get 100 free contact syncs added to test out the plugin before your buy. <br /><br />
                            </small>
                            <label>What is a Contact Sync?</label>
                            <small>Each time that you merge content from your blog into a single contact in ONTRAPORT using Bulletini's Sync Engine, this is counted as a content sync. If you have 500 contacts and you want to send new content to them 4 times in a calendar month you will require an account with 2000 or more Contact Syncs per month. Once you hit your monthly limit the Sync Engine will be disabled until the start of the next calendar month. </small>
                            <label for="bulletini_lite_license_key">Your License Key</label>
                            <div class="input_area">
                            	<input type="text" name="bulletini_lite_license_key" id="bulletini_lite_license_key" value="<?php echo esc_attr($bulletini_lite_license_key)?>" class="bulletini_lite_inputbox" />
                                <?php
								$message=bulletini_lite_option("message");
								if($message!=""){
									$this->add_notice($message, 'information', 'license');
								}
								$this->print_notices('license');
								?>
                           	</div>
                            <?php submit_button('Click here to refresh');?>
                      	</div>
                   	</div>
					<?php
                    if(bulletini_lite_authenticated()){
						?>
						<div id="content-configuration" class="content-elements">
							<h4>API Configuration</h4>
							<div class="form-table">
                            	<label>Connect to ONTRAPORT</label>
                                <small>Find your ONTRAPORT API information in the administration section of your app. <a href="https://support.ontraport.com/hc/en-us/articles/217882248-API-in-ONTRAPORT" target="_blank">Click here for help on how to find your API</a>.</small>
                                <?php $bulletini_lite_app_id=bulletini_lite_option('app_id');?>
								<label for="bulletini_lite_app_id">APP ID</label>
								<div class="input_area">
									<input type="text" name="bulletini_lite_app_id" id="bulletini_lite_app_id" value="<?php echo esc_attr($bulletini_lite_app_id)?>" class="bulletini_lite_inputbox" />
								</div>
								<?php $bulletini_lite_api_key=bulletini_lite_option('api_key');?>
								<label for="bulletini_lite_api_key">API Key</label>
								<div class="input_area">
									<input type="text" name="bulletini_lite_api_key" id="bulletini_lite_api_key" value="<?php echo esc_attr($bulletini_lite_api_key)?>" class="bulletini_lite_inputbox" />
								</div>
                                <?php $this->print_notices('ConnectToONTRAPORT')?>
								<?php
								if(!$ontraport->is_valid()){
									submit_button();
								}
								?>
							</div>
						</div>
						<?php
						if($ontraport->is_valid()){
							?>
                            <div id="content-content-settings" class="content-elements">
                                <h4>Content Type Settings</h4>
                                <div class="form-table">
                                    <label>Content Type Settings</label>
                                    <small>In this section you are able to decide which post or page type from your WP site you will be using as content in Bulletini. If you have custom fields in your content then you can decide which fields you want to be using for each piece of content.</small>
                                    <?php $bulletini_lite_post_type=bulletini_lite_option('post_type', false, 'post');?>
                                    <label for="bulletini_lite_post_type">Post Type</label>
                                    <div class="input_area">
                                        <select name="bulletini_lite_post_type" id="bulletini_lite_post_type" class="bulletini_inputbox bulletini_lite_selectbox">
                                            <?php
                                            $bulletini_lite_post_types=get_post_types();
                                            foreach($bulletini_lite_post_types as $post_type){
                                                ?>
                                                <option value="<?php echo $post_type?>"<?php echo $bulletini_lite_post_type==$post_type?' selected="selected"':''?>><?php echo $post_type?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                        <small>Listed above are all the content types you have in your Wordpress Site. Most blogs use 'Posts' for their content. We have allowed for the possibility that you may have a different post type that you want to use in your content.</small>                     	
                                    </div>
                                    <label>Content Fields</label>
                                    <small>Each piece of content that is used in your Newsletter can have 5 fields associated. Title, Photo, Description, Author, Link. Bulletini is setup already by default to match the standard Wordpress content fields from blog posts however if you have other custom fields you are using in your posts, then this is where you can override the standard settings.<br /><br />
                                    Use the dropdown fields below to choose which Wordpress Data point you want to associate to each of the 5 field types that will be created in ONTRAPORT for each piece of content.</small>
                                    <?php
									$default_content_fields=array(
                                        "title"=>"post_title",
                                        "image"=>"post_thumbnail",
                                        "desc"=>"post_content",
                                        "author"=>"post_author",
                                        "link"=>"permalink",
                                    );
                                    foreach(array(
                                        "title"=>"Title",
                                        "image"=>"Photo",
                                        "desc"=>"Description",
                                        "author"=>"Author",
                                        "link"=>"Link",
                                    ) as $k=>$v){
                                        $bulletini_lite_field=bulletini_lite_option($k.'_field', false, $default_content_fields[$k]);?>
                                        <label for="bulletini_<?php echo $k?>_field"><?php echo $v?> Field</label>
                                        <div class="input_area">
                                            <?php $this->field_select_box($k.'_field', $bulletini_lite_field)?>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <div id="content-templates" class="content-elements">
                                <h4>Templates</h4>
                                <div class="form-table">
                                    <label>ONTRAPORT HTML Message Templates</label>
                                    <small>Bulletini's sync engine will take blog content from your Wordoress posts and add it into your ONTRAPORT contacts based on what each contact has listed under their interests. The Bulletini fields can then be used in any one of your messages to merge the relevant content in. <br /><br />
                                    Bulletini has configurable templates below that you can push into your account that are already setup with the correct Bullletini content merge fields.<br /><br />
                                    If you have your own design in mind then you can use these templates as a guide and then edit them within your ONTRAPORT account to make them as customized as needed.</small>
                                    <?php
                                    $template_path=plugin_dir_path( __FILE__ ).'../templates';
									$templates = scandir($template_path);
									sort($templates);
									foreach ($templates as $template) {
                                        if ($template != "." && $template != ".." && is_dir($template_path."/".$template)) {
											$bulletini_lite_template_message_id=bulletini_lite_option('template_'.$template.'_message_id');
											$config_data=array();
											$config_file=plugin_dir_path( __FILE__ ).'../templates/'.$template."/config.xml";
											if(file_exists($config_file)){
												$config_data=simplexml_load_string(file_get_contents($config_file));
												if(!$config_data){
													$config_data=array();
												}
												else{
													$config_data=(array)$config_data;
												}
											}
											?>
											<div class="bulletini-template-container clearfix">
												<label style="display:none" for="bulletini_lite_post_type"><?php echo isset($config_data["name"])?$config_data["name"]:$template;?></label>
												<div class="input_area">
													<?php $this->print_notices('Template'.$template);?>
													<div class="bulletini-template-thumbnail"><a href="<?php echo plugin_dir_url( __FILE__ ).'../templates/'.$template."/screenshot.png"?>" target="_blank"><img src="<?php echo plugin_dir_url( __FILE__ ).'../templates/'.$template."/screenshot.png"?>" /></a></div>
													<div class="bulletini-template-details">
														<ul>
															<li><span class="bulletini-template-label">Template Name: </span> <?php echo isset($config_data["name"])?$config_data["name"]:$template;?></li>
															<li><p><?php echo isset($config_data["description"])?$config_data["description"]:'';?></p></li>
															<li><a class="button button-primary bulletini_lite_template_options_page" href="#bulletini_lite_fancybox_wrapper_<?php echo $template?>" data-template="<?php echo $template?>">Settings</a>
																<?php
																if(!empty($bulletini_lite_template_message_id)){
																	?>
																	<a class="button button-primary" href="https://app.ontraport.com/#!/message/edit&id=<?php echo $bulletini_lite_template_message_id?>" target="_blank">View Last Message</a>
																	<?php
																}
																?>
																<a class="button button-primary" href="<?php echo wp_nonce_url( admin_url('admin.php?page=bulletini-lite&bulletini_lite_install_template&template='.$template), 'install-template_'.$template, 'bulletini_lite_install_template_nonce' );?>#content-templates">Install</a></li>
														</ul>
													</div> 
												</div>
												<div style="display:none">
													<div class="bulletini_lite_fancybox_wrapper" id="bulletini_lite_fancybox_wrapper_<?php echo $template?>">
														<div class="bulletini_lite-fields not-expanded">
															<h3>Settings<span></span></h3>
															<input type="hidden" name="bulletini_lite_template" id="bulletini_lite_template" value="<?php echo $template?>" />
															<div class="form-table">
																<?php
																if(isset($config_data["configuration"])){
																	foreach($config_data["configuration"] as $field_type=>$field_key){
																		$field_input_key='bulletini_lite_'.$template."_".str_replace("-", "_", sanitize_title($field_key));
																		?>
																		<label for="<?php echo $field_input_key?>"><?php echo $field_key?></label>
																		<div class="input_area">
																			<?php
																			switch($field_type){
																				case "text":
																					?>
																					<input type="text" name="<?php echo $field_input_key?>" id="<?php echo $field_input_key?>" value="<?php echo esc_attr(get_option($field_input_key))?>" />
																					<?php
																				break;
																				case "color":
																					?>
																					<input type="text" name="<?php echo $field_input_key?>" id="<?php echo $field_input_key?>" value="<?php echo esc_attr(get_option($field_input_key))?>" class="wct-colorpicker" />
																					<?php
																				break;
																				case "textarea":
																					?>
																					<textarea name="<?php echo $field_input_key?>" id="<?php echo $field_input_key?>" ><?php echo esc_textarea(get_option($field_input_key))?></textarea>
																					<?php
																				break;
																				case "image":
																					$this->media_upload($field_input_key, esc_attr(get_option($field_input_key)));
																				break;
																				case 'posts':
																					?>
																					<div class="posts_selection_box">
                                                                                    	<input type="hidden" name="<?php echo $field_input_key?>" id="<?php echo $field_input_key?>" value="<?php echo esc_attr(get_option($field_input_key))?>">
                                                                                    	<div class="selected_posts"></div>
                                                                                        <div class="search_input_box">
                                                                                        	<input type="text" placeholder="Search posts here by typing some characters..." />
                                                                                       	</div>
                                                                                        <div class="search_result_box"></div>
                                                                                    </div>
																					<?php
																				break;
																			}
																			?>
																		</div>	
																		<?php
																	}
																	?>
																	<div class="clr"></div>
																	<div class="info_bar info_bar_bottom"><a class="button button-primary bulletini_lite-save-settings" href="#">Save Settings</a></div>
																	<?php
																}
																else{
																	?>
																	<div class="message">No Settings defined for this template</div>
																	<?php
																}
																?>
															</div>
														</div>
													</div>
													<div class="clr"></div>
												</div>
											</div>
											<?php
                                        }
                                    }                            
                                    ?>
                                </div>
                            </div>
                            <?php
						}
					}
					?>
           		</div>
                <div class="clr"></div>
                <?php $this->print_notices('general')?>
                <div class="info_bar info_bar_bottom">
                	<?php submit_button();?>
              	</div>
            </form>
        </div>
        <?php
    }
	public function field_select_box($key, $value){
		global $wpdb;
		if(count($this->post_fields)==0){
			$post_type=bulletini_lite_option('post_type', false, 'post');
			$query = "
				SELECT DISTINCT($wpdb->postmeta.meta_key) 
				FROM $wpdb->posts 
				LEFT JOIN $wpdb->postmeta 
				ON $wpdb->posts.ID = $wpdb->postmeta.post_id 
				WHERE $wpdb->posts.post_type = '%s' 
				AND $wpdb->postmeta.meta_key != '' 
				AND $wpdb->postmeta.meta_key NOT RegExp '(^[_0-9].+$)' 
				AND $wpdb->postmeta.meta_key NOT RegExp '(^[0-9]+$)'
			";
			$this->post_fields = $wpdb->get_col($wpdb->prepare($query, $post_type));
		}
		?>
		<select name="bulletini_lite_<?php echo $key?>" id="bulletini_lite_<?php echo $key?>" class="bulletini_lite_inputbox bulletini_lite_selectbox">
			<option value="post_title"<?php echo $value=="post_title"?' selected="selected"':''?>>Post Title</option>
            <option value="post_content"<?php echo $value=="post_content"?' selected="selected"':''?>>Post Content</option>
            <option value="post_thumbnail"<?php echo $value=="post_thumbnail"?' selected="selected"':''?>>Post Thumbnail</option>
            <option value="post_author"<?php echo $value=="post_author"?' selected="selected"':''?>>Post Author</option>
            <option value="permalink"<?php echo $value=="permalink"?' selected="selected"':''?>>Permalink</option>
			<?php
            foreach($this->post_fields as $field_key){
				?>
				<option value="<?php echo $field_key?>"<?php echo $value==$field_key?' selected="selected"':''?>><?php echo $field_key?></option>
				<?php
			}
			?>
        </select>
		<?php
	}
	public function media_upload($field_id, $field_value, $type='image', $post_id=null){
		$have_img=!empty($field_value);
		$upload_link = esc_url( get_upload_iframe_src($type,$post_id));
		?>
        <div class="bulletini-upload-container">
            <div class="bulletini-image-container">
                <?php
                if($have_img){
                    $img_src = wp_get_attachment_image_src( $field_value, 'full' );
                    ?>
                    <a href="<?php echo $img_src[0] ?>" target="_blank"><img src="<?php echo $img_src[0] ?>" alt="" /></a>
                    <?php
                }
                ?>
            </div>
            <p class="hide-if-no-js" data-fieldid="<?php echo $field_id?>">
                <a class="bulletini-upload-add-button button <?php if ($have_img) { echo 'hidden'; } ?>" 
                   href="<?php echo $upload_link ?>">
                    <?php _e('Set Image') ?>
                </a>
                <a class="bulletini-upload-delete-button button <?php if (!$have_img) { echo 'hidden'; } ?>" 
                  href="#">
                    <?php _e('Remove this image') ?>
                </a>
            </p>                                
            <input id="<?php echo $field_id?>" name="<?php echo $field_id?>" type="hidden" value="<?php echo esc_attr( $field_value ); ?>" class="bulletini-upload-field-id" />
      	</div>
        <?php
	}
	public function add_notice($text, $type='message', $location='general'){
		if(!isset($this->notices[$location][$type])){
			$this->notices[$location][$type]=array();
		}
		if(!in_array($text, $this->notices[$location][$type])){
			$this->notices[$location][$type][]=$text;
		}
	}
	public function print_notices($location='general'){
		if(isset($this->notices[$location])){
			$notices=$this->notices[$location];
			if(isset($notices["error"])){
				foreach($notices["error"] as $notice){
					?>
					<div class="buttetini_notice buttetini_err"><?php echo $notice?></div>
					<?php
				}
			}
			if(isset($notices["message"])){
				foreach($notices["message"] as $notice){
					?>
					<div class="buttetini_notice buttetini_msg"><?php echo $notice?></div>
					<?php
				}
			}
			if(isset($notices["information"])){
				foreach($notices["information"] as $notice){
					?>
					<div class="buttetini_notice buttetini_info"><?php echo $notice?></div>
					<?php
				}
			}
		}
	}
	public function option($option_name, $apply_the_content=false, $default_value=""){
		$value=get_option("bulletini_lite_".$option_name, "");
		if($value==""){
			$value=$default_value;
		}
		if($apply_the_content){
			$value=apply_filters('the_content', $value);
		}
		return $value;
	}
}
