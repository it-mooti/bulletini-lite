jQuery(window).load(function(){
	jQuery('.bulletini_lite_selectbox').trigger('chosen:updated');
});
function bulletini_lite_build_box( container, index, id, title ){
	container.find( '.selected_posts' ).append('<div class="selected_post">'+title+' <span class="remove_selected_post" data-id="'+index+'">x</span></div>');
	init_remove_selected_post();
}
function init_remove_selected_post(){
	jQuery( '.remove_selected_post' ).unbind( 'click' ).click(function(){
		var $search_box = jQuery(this).parents( '.posts_selection_box' );
		var $input = $search_box.find( "input[type='hidden']" );
		var index = jQuery( this ).data( 'index' );
		if( $input.val() != '' ) {
			var post_ids = $input.val().split( "," );
			post_ids.splice( index, 1 ); 
			$input.val( post_ids.join() );
		}
		jQuery( this ).parent().remove();
	});
}

function init_search_result_item(){
	jQuery( '.search_result_item' ).unbind( 'click' ).click(function(){
		var $search_box = jQuery(this).parents( '.posts_selection_box' );
		var $input = $search_box.find( "input[type='hidden']" );
		if( $input.val() != '' ) {
			var post_ids = $input.val().split( "," );
		}
		else{
			var post_ids = [];
		}
		post_ids.push( jQuery(this).data( 'id' ) );
		$search_box.find( "input[type='hidden']" ).val( post_ids.join() );
		bulletini_lite_build_box( $search_box, post_ids.length, jQuery(this).data( 'id' ), jQuery(this).data( 'title' ) );
		$search_box.find( '.search_result_box' ).html( '' );
		$search_box.find( ".search_input_box input" ).val( '' );
	});
}
jQuery(document).ready(function(){
	jQuery( ".posts_selection_box" ).each(function(){
		var $search_box = jQuery(this);
		var $input = $search_box.find( "input[type='hidden']" );
		if( $input.val() != '' ) {
			var post_ids = $input.val().split( "," );
			for( var i = 0; i < post_ids.length; i++ ){
				jQuery.post( $bulletini_lite_ajax_object.ajax_url, {action: 'bulletini_lite_get_post_title', id: post_ids[ i ]}, function( response ) {
					if( response != '' ) {
						bulletini_lite_build_box( $search_box, i, post_ids[ i ], response );
					}
				} );
			}
		}
	});
	var wct_xhr;
	jQuery( ".search_input_box input" ).keyup(function(){
		var s = jQuery(this).val();
		var p = jQuery(this).parents('.posts_selection_box');
		if( wct_xhr ) {
			wct_xhr.abort();
		}
		if( s != '' ) {
			p.find( '.search_result_box' ).html( '<div class="msg">Searching...</div>' );
			wct_xhr = jQuery.post( $bulletini_lite_ajax_object.ajax_url, {action: 'bulletini_lite_get_posts', s: s, exclude: p.find( 'input[type="hidden"]' ).val()}, function( response ) {
				response = JSON.parse( response );
				if( response.length > 0 ) {
					p.find( '.search_result_box' ).html( '' );
					for( var i = 0; i < response.length; i++ ){
						p.find( '.search_result_box' ).append( '<div class="search_result_item" data-id="'+response[ i ].id+'" data-title="'+response[ i ].title+'">'+response[ i ].title+'</div>' );
					}
					init_search_result_item();
				}
				else{
					p.find( '.search_result_box' ).html( '<div class="err">No matches found.<div>' );
				}
			} );
		}
		else{
			p.find( '.search_result_box' ).html( '' );
		}
	});
	jQuery('.bulletini_lite_selectbox').chosen({"width": "100%"});
	jQuery('.bulletini_lite_template_options_page').fancybox({type: 'inline'});
	jQuery(".bulletini_lite-fields .content-elements").hide();
	jQuery(".bulletini_lite-fields .content-elements:first-child").addClass('current').show();
	jQuery(".left-links li:first-child").addClass("current");
	jQuery(".left-links a").click(function(e){
		e.preventDefault();
		document.location.hash=jQuery(this).attr("href");
		jQuery(".left-links li").removeClass("current");
		jQuery(this).parent().addClass("current");
		jQuery(".bulletini_lite-fields .content-elements.current").removeClass("current").hide();
		jQuery(".bulletini_lite-fields "+jQuery(this).attr("href")).addClass("current").show();
	});
	jQuery(".left-links a.bulletini_lite_external_link").unbind('click');
	jQuery(".bulletini_lite_toggle").change(function(){
		$bulletini_lite_toggle_selector=jQuery(this).data("target");
		if(jQuery(this).is(":checked") && jQuery(this).val()!=""){
			jQuery($bulletini_lite_toggle_selector).slideDown();
		}
		else{
			jQuery($bulletini_lite_toggle_selector).slideUp();
		}
	});
	jQuery(".bulletini_lite_datefield").datepicker();
	jQuery(".bulletini_lite_cron_schedule").change(function(){
		$val=jQuery(this).find("option:selected").val();
		jQuery(".bulletini_lite_cron_schedule_settings").hide();
		jQuery(".bulletini_lite_cron_schedule_settings.bulletini_lite_cron_schedule_"+$val).show();
	}).change();
	jQuery("#expand_options").click(function(){
		if(jQuery(this).hasClass('close')){
			jQuery(this).removeClass("close").addClass("expand");
			jQuery(this).parents(".bulletini_lite-fields").removeClass("expanded").addClass("not-expanded");
			jQuery(this).parents(".bulletini_lite-fields").find(".content-elements").not(".current").hide();
		}
		else{
			jQuery(this).removeClass("expand").addClass("close");
			jQuery(this).parents(".bulletini_lite-fields").removeClass("not-expanded").addClass("expanded");
			jQuery(this).parents(".bulletini_lite-fields").find(".content-elements").show();
		}
	});
	$hash_value=window.location.href.split("#");
	if($hash_value[1]){
		if(jQuery(".left-links a[href='#"+$hash_value[1]+"']").length>0){
			jQuery(".left-links a[href='#"+$hash_value[1]+"']").trigger("click");
		}
	}
	if (jQuery('.bulletini_lite-upload-add-button').length > 0) {
		if ( typeof wp !== 'undefined' && wp.media && wp.media.editor) {
			jQuery('.bulletini_lite-upload-add-button').on( 'click', function( event ){
				var frame;
				event.preventDefault();
				if ( frame ) {
					frame.open();
					return;
				}
				metaBox = jQuery(this).parents('.bulletini_lite-upload-container'), // Your meta box id here
			  	imgContainer = metaBox.find( '.bulletini_lite-image-container'),
			  	imgIdInput = metaBox.find( '.bulletini_lite-upload-field-id' );
				addImgLink = jQuery(this),
      			delImgLink = metaBox.find( '.bulletini_lite-upload-delete-button'),
				frame = wp.media({
					title: 'Select or Upload Media Of Your Chosen Persuasion',
					button: {
						text: 'Use this media'
					},
					multiple: false  // Set to true to allow multiple files to be selected
				});
				frame.on( 'select', function() {
					var attachment = frame.state().get('selection').first().toJSON();
					imgContainer.append( '<a href="'+attachment.url+'" target="_blank"><img src="'+attachment.url+'" alt="" /></a>' );
					imgIdInput.val( attachment.id );
					addImgLink.addClass( 'hidden' );
					delImgLink.removeClass( 'hidden' );
				});
				frame.open();
			});
			jQuery('.bulletini_lite-upload-delete-button').on( 'click', function( event ){
				event.preventDefault();
				metaBox = jQuery(this).parents('.bulletini_lite-upload-container'), // Your meta box id here
			  	imgContainer = metaBox.find( '.bulletini_lite-image-container'),
			  	imgIdInput = metaBox.find( '.bulletini_lite-upload-field-id' );
				addImgLink = metaBox.find( '.bulletini_lite-upload-add-button'),
      			delImgLink = jQuery(this);
				imgContainer.html( '' );
				addImgLink.removeClass( 'hidden' );
				delImgLink.addClass( 'hidden' );
				imgIdInput.val('');
			});
		}
	}
	if(jQuery('.bulletini_lite-save-settings').length>0){
		jQuery('.wct-colorpicker').wpColorPicker();
		jQuery('.bulletini_lite-save-settings').click(function(e){
			e.preventDefault();
			$bulletini_lite_btn=jQuery(this);
			$bulletini_lite_box=jQuery(this).parents('.bulletini_lite_fancybox_wrapper');
			$bulletini_lite_btn.hide();
			$bulletini_lite_btn.before('<span class="bulletini_lite_loading">Submitting...   </span>');
			$bulletini_lite_data={
				'action': 'bulletini_lite_save_template_settings'
			};
			$bulletini_lite_box.find(":input").each(function(){
				$input=jQuery(this);
				$bulletini_lite_data[$input.attr("id")]=$input.val();
			});
			jQuery.post($bulletini_lite_ajax_object.ajax_url, $bulletini_lite_data, function($bulletini_lite_response) {
				$bulletini_lite_message_container=$bulletini_lite_box.find('.bulletini_lite_message');
				if($bulletini_lite_message_container.length>0){
					$bulletini_lite_btn.before('<span class="bulletini_lite_message"></span>');
				}
				if($bulletini_lite_response==1){
					$bulletini_lite_message_container.html('Settings Saved!   ');
				}
				else{
					$bulletini_lite_message_container.html('Error is saving settings. Try again!   ');
				}
				$bulletini_lite_box.find('.bulletini_lite_loading').remove();
				$bulletini_lite_btn.show();
			});
		});
	}
});
angular.module('bulletini_liteLog', [])
  	.controller('bulletini_liteLogController', function ($scope, $http) {
		var bulletini_liteLogInstance=this;
		bulletini_liteLogInstance.logrecords = [];
		bulletini_liteLogInstance.page=1;
		bulletini_liteLogInstance.loading=false;
		bulletini_liteLogInstance.addRecords = function(records) {
			bulletini_liteLogInstance.logrecords=records;
		}
		bulletini_liteLogInstance.getRecords = function() {
			bulletini_liteLogInstance.loading=true;
			$http({
				method: 'POST',
				url: $bulletini_lite_ajax_object.ajax_url,
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				transformRequest: function(obj) {
					var str = [];
					for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
					return str.join("&");
				},
				data: {action: 'bulletini_lite_get_sync_log', page: bulletini_liteLogInstance.page, rand: Math.random()}
			}).success(function ($response) {
				bulletini_liteLogInstance.addRecords($response);
				bulletini_liteLogInstance.loading=false;
			});
		}
		bulletini_liteLogInstance.nextRecords = function() {
			bulletini_liteLogInstance.page++;
			bulletini_liteLogInstance.getRecords();
		}
		bulletini_liteLogInstance.prevRecords = function() {
			bulletini_liteLogInstance.page--;
			bulletini_liteLogInstance.getRecords();
		}
		bulletini_liteLogInstance.getRecords();
	}
)