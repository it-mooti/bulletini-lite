<?php
class bulletini_lite_ontraport{
	private $appId, $apiKey, $url, $contactFields = null;
	public $objectTypes=array(
		"0"=>"contact",
		"14"=>"tag",
	);
	public function __construct($appId, $apiKey){
		$this->appId=$appId;
		$this->apiKey=$apiKey;
		$isSecure = false;
		if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
			$isSecure = true;
		}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on') {
			$isSecure = true;
		}
		$this->url=($isSecure ? 'https' : 'https')."://api.ontraport.com/1";
	}
	public function is_valid(){
		$response=$this->Request('objects/getInfo', array('objectID'=>$this->getObjectID("contact")));
		if(!is_object($response)){
			return false;
		}
		return true;
	}
	public function searchContact($condition='', $keyword=''){
		$parameters=array("objectID"=>$this->getObjectID("contact"));
		if(is_array($condition)){
			$query='';
			foreach($condition as $k=>$v){
				$query.=$k."='".$v."'&";
			}
			$query=rtrim($query, '&');
			$condition=$query;
		}
		if(!empty($condition)){
			$parameters["condition"]=$condition;
		}
		if(!empty($search)){
			$parameters["search"]=$search;
		}
		$contacts=$this->Request('objects', $parameters);
		return $contacts;
	}
	public function findContactByEmail($email){
		$contact=$this->searchContact("email='".$email."'");
		if(is_array($contact)){
			$contact=$contact[0];
		}
		else{
			$conact=false;
		}
		return $contact;
	}
	public function getContactByID($id){
		$contact="";
		$object=$this->Request('object', array('objectID'=>$this->getObjectID("contact"), "id"=>$id));
		if($object){
			$contact=$object;
		}
		return $contact;
	}
	public function saveUserData($contactID, $fields){
		return $this->saveObject("contact", $contactID, $fields);
	}
	public function saveObject($objectType, $objectID, $fields){
		$parameters=array_merge(array(
			"objectID"=>$this->getObjectID($objectType)
		), $fields);
		$method="post";
		if($objectID>0){
			$parameters["id"]=$objectID;
			$method="put";
		}
		return $this->Request('objects', $parameters, true, $method);
	}
	public function getObjectID($objectName){
		return array_search($objectName, $this->objectTypes);
	}
	public function getTagName($tagID){
		$tagName="";
		$tag=$this->Request('object', array('objectID'=>$this->getObjectID("tag"), "id"=>$tagID));
		if($tag){
			$tagName=$tag->tag_name;
		}
		return $tagName;
	}
	public function getTagsArray($contactCat){
		$tags=array();
		if(!empty($contactCat)){
			$tagIDs=explode('*/*', trim($contactCat, '*/*'));
			foreach($tagIDs as $tagID){
				$tag=$this->getTagName($tagID);
				$tags[$tagID]=$tag;
			}
		}
		return $tags;
	}
	public function getContactFields($refresh=0){
		if (is_null($this->contactFields) || $refresh==1) {
            $this->contactFields=$this->Request('objects/meta', array('objectID'=>$this->getObjectID("contact")));
			if(isset($this->contactFields->{0}->fields)){
				$this->contactFields=$this->contactFields->{0}->fields;
			}
        }
        return $this->contactFields;
	}
	public function getFieldAlias($fieldKey){
		$contact_field=$this->getContactFields();
		if(!empty($fieldKey) && isset($contact_field->$fieldKey)){
			return $contact_field->$fieldKey->alias;
		}
		return "";
	}
	public function getDropOption($field_id, $id){
		$contact_fields=$this->getContactFields();
		if(isset($contact_fields->$field_id->options->$id)){
			return $contact_fields->$field_id->options->$id;
		}
	}
	public function getTags(){
		$tags=array();
		try{
			$start=0;
			do{
				$tags_loop=$this->Request('objects', array('objectID'=>$this->getObjectID('tag'), "start"=>$start));
				if(is_array($tags_loop)){
					$tags=array_merge($tags, $tags_loop);
					$start+=50;
				}
				else{
					$tags_loop=array();
				}
			} while(count($tags_loop)==50);
		}catch(Exception $e){
			error_log("Error in Fetching contact records");
		}
		return $tags;
	}
	public function Request($object, $parameters = array(), $post=false, $request_type="get"){
		if(empty($this->appId) || empty($this->apiKey)){
			return false;
		}
		if($post && $request_type=="get"){
			$request_type="post";
		}
		if(in_array($object, array("cdata", "fdata", "pdata"))){
			$parameters = array_merge(array("appid"=>$this->appId, "key"=>$this->apiKey), $parameters);
			$post_parameters=http_build_query($parameters, '', '&');
			$request = "https://api.ontraport.com/".$object.".php";
			$args=array(
				'method'=>'POST',
				'timeout'=>60,
				'body' => $post_parameters,
				'sslverify'=>false,
			);
			$response=wp_remote_request($request, $args);
			if ( is_wp_error( $response ) ) {
			   $error_message = $response->get_error_message();
			   bulletini_lite_add_notice("Something went wrong: $error_message", "error");
			} else {
				libxml_use_internal_errors(true);
				$result = simplexml_load_string($response["body"]);
			   	if($result){
					return $result;
				}
			}
			return false;
		}
		
		//New API
		$headers = array(
			'Api-Appid'=>$this->appId,
			'Api-Key'=>$this->apiKey
		);
		$args=array(
			'timeout'=>60,
			"headers"=>$headers,
			'sslverify'=>false,
		);
		$url=$this->url.'/'.$object;
		if(is_array($parameters) && count($parameters)>0){
			if($post){
				$post_parameters=http_build_query($parameters, '', '&');
				$args["body"]=$post_parameters;
			}
			else{
				$url.='?'.http_build_query($parameters, '', '&');
			}
		}
		$args["method"]=strtoupper($request_type);
		$response=wp_remote_request($url, $args);
		if ( is_wp_error( $response ) ) {
		   $error_message = $response->get_error_message();
		   bulletini_lite_add_notice("Something went wrong: $error_message", "error");
		} else {
			$result = json_decode($response["body"]);
			if (json_last_error() === JSON_ERROR_NONE) {
				if(!isset($result->code) || $result->code!="0"){
					bulletini_lite_add_notice("Error: Error Code Number: ".$result->code, "error");
					return false;
				}
				if(isset($result->data) && count($result->data)>0){
					return $result->data;
				}
				return true;
			}
			else{
				bulletini_lite_add_notice("Ontraport API Error: ".$response["body"], "error");
				return $response["body"];
			}
		}
		return false;
	}
}