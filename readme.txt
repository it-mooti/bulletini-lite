﻿=== Bulletini Lite ===
Contributors: ITMOOTI
Donate link: http://www.itmooti.com/
Tags: ontraport, newsletter
Requires at least: 4.0
Tested up to: 4.9.5
Stable tag: 1.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Send Tailored WordPress Content To Your ONTRAPORT Contacts.

== Description ==

A simple to use plugin that brings relevant content from your Wordpress Blog into ONTRAPORT contact records ready to be merged into a beautifully designed bulletin newsletter.

== Installation ==

1. Upload `bulletini` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

= Learn More In Our Knowledge Base =

<a href="https://itmooti.helpdocs.com/bulletini">Bulletini</a>

== Screenshots ==

== Upgrade Notice ==

Nothing yet.

= 1.0 =

First Release
